import QtQuick 2.7
import QtQuick.Controls 2.2
import QtQuick.Layouts 1.3
import QtLocation 5.9
import QtPositioning 5.8

ApplicationWindow {
    id: root
    visible: true
    width: 640
    height: 480
    title: qsTr("Position")

    property string validLatColor : "white"
    property string validLonColor : "white"
    property string validAltitudeColor : "white"
    property string validSpeedColor : "white"
    property string validDirectionColor : "white"
    property string validHorizontalAccuracyColor : "white"
    property string validVerticalAccuracyColor : "white"

    property string lat: "   -.- °"
    property string lon: "   -.- °"
    property string altitude: "   - m"
    property string speed: "   -.- m/sec"
    property string speedKmH: "   -.- km/h"
    property string direction: "   -.- °"
    property string horizontalAccuracy: "   - m"
    property string verticalAccuracy: "   - m"

    property bool bMovingMap: false

    //! [Current Location]
    PositionSource {
        id: positionSource
        active: true
        updateInterval: 200 //
        onPositionChanged:  {

            if (position.latitudeValid == true) {
                validLatColor = "lightgreen"
                lat = position.coordinate.latitude.toFixed(6) +" °"
            }
            else  {
                validLatColor = "red"
                lat = "   -.- °"
            }

            if (position.longitudeValid == true) {
                validLonColor = "lightgreen"
                lon = position.coordinate.longitude.toFixed(6) +" °"
            }
            else  {
                validLonColor = "red"
                 lon = "   -.- °"
            }

            if (position.altitudeValid == true) {
                validAltitudeColor = "lightgreen"
                altitude = position.coordinate.altitude.toFixed(0) +" m"

            }
            else  {
                validAltitudeColor = "red"
                altitude = "   - m"
            }

            if (position.speedValid == true) {
                validSpeedColor = "lightgreen"
                speed = position.speed.toFixed(1) +" m/sec"
                speedKmH = (position.speed*3.6).toFixed(1) +" km/h"
            }
            else  {
                validSpeedColor = "red"
                speed = "   - m/sec"
                speedKmH = "   - km/h"
            }
//              speedKmH = "222.2 km/h"

            if (position.directionValid == true) {
                validDirectionColor = "lightgreen"
                direction = position.direction.toFixed(1) +"°"
            }
            else  {
                validDirectionColor = "red"
               direction = "   - °"
            }
//             direction = "333.3°"

            if (position.horizontalAccuracyValid == true) {
                validHorizontalAccuracyColor = "lightgreen"
                horizontalAccuracy = position.horizontalAccuracy.toFixed(1) +" m"
            }
            else  {
                validHorizontalAccuracyColor = "red"
                horizontalAccuracy = "   -  m"
            }

            if (position.verticalAccuracyValid == true) {
                validVerticalAccuracyColor = "lightgreen"
                verticalAccuracy = position.verticalAccuracy.toFixed(1) +" m"
            }
            else  {
                validVerticalAccuracyColor = "red"
                verticalAccuracy = "   -  m"
            }
        }
    }
    //! [Current Location]



    SwipeView {
        id: swipeView
        anchors.fill: parent
        currentIndex: tabBar.currentIndex

        PageMap {
            Label {

            }
        }

        Page0 {
            Label {

            }
        }

        Page1 {
            Label {

            }
        }

        Page3 {
            Label {

            }
        }

        Page2 {
            Label {

            }
        }
    }

    footer: TabBar {
        id: tabBar
        currentIndex: swipeView.currentIndex

        TabButton {
            text: qsTr("Map")
        }

        TabButton {
            text: qsTr("Speed")
        }

        TabButton {
            text: qsTr("GNSS")

        }

        TabButton {
            text: qsTr("Tools")

        }

        TabButton {
            text: qsTr("About")
        }
    }
}
