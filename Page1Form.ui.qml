import QtQuick 2.7
import QtQuick.Controls 2.2
import QtQuick.Layouts 1.3
import QtPositioning 5.8
import QtLocation 5.9

Item {
    width: 640
    height: 480
    property alias labelLat: labelLat
    property alias labelLon: labelLon
    property alias labelAltitude: labelAltitude
    property alias validVAccuracy: validVAccuracy
    property alias validHAccuracy: validHAccuracy
    property alias validDirection: validDirection
    property alias validSpeed: validSpeed
    property alias validAltitude: validAltitude
    property alias validLon: validLon
    property alias validLat: validLat
    property alias labelVAccuracy: labelVAccuracy
    property alias labelHAccuracy: labelHAccuracy
    property alias labelSpeed: labelSpeed
    property alias labelDirection: labelDirection

    RowLayout {
        anchors.horizontalCenter: parent.horizontalCenter
        anchors.topMargin: 20
        anchors.top: parent.top
    }

    //Latitude
    Label {
        id: label1
        anchors.top: parent.top
        anchors.topMargin: (parent.height-140)/8
        anchors.right: parent.horizontalCenter
        anchors.rightMargin: 40
        width: 110
        height: 20
        text: qsTr("Latitude")
        verticalAlignment: Text.AlignVCenter
        horizontalAlignment: Text.AlignRight
        font.pixelSize: parent.height/20
    }
    Label {
        id: labelLat
        anchors.left: validLat.right
        anchors.leftMargin: 10
        anchors.verticalCenter: label1.verticalCenter
        width: 120
        height: 20
        text: qsTr("   -.-")
        verticalAlignment: Text.AlignVCenter
        horizontalAlignment: Text.AlignLeft
        font.pixelSize: parent.height/20
    }
    Rectangle {
        id: validLat
        anchors.left: label1.right
        anchors.leftMargin: 10
        anchors.verticalCenter: label1.verticalCenter
        width: 24
        height: 24
        color: "red"
        radius: 12
        border.width: 2
        border.color: "#000000"
    }

    //Longitude
    Label {
        id: label2
        anchors.top: label1.bottom
        anchors.topMargin: (parent.height-140)/8
        anchors.right: parent.horizontalCenter
        anchors.rightMargin: 40
        y: 74
        width: 110
        height: 20
        text: qsTr("Longitude")
        verticalAlignment: Text.AlignVCenter
        horizontalAlignment: Text.AlignRight
        font.pixelSize: parent.height/20
    }
    Label {
        id: labelLon
        anchors.left: validLon.right
        anchors.leftMargin: 10
        anchors.verticalCenter: label2.verticalCenter
        width: 120
        height: 20
        text: qsTr("   -.-")
        verticalAlignment: Text.AlignVCenter
        font.pixelSize: parent.height/20
    }
    Rectangle {
        id: validLon
        anchors.left: label2.right
        anchors.leftMargin: 10
        anchors.verticalCenter: label2.verticalCenter
        width: 24
        height: 24
        color: "#ff0000"
        radius: 12
        border.color: "#000000"
        border.width: 2
    }

    //Altitude
    Label {
        id: label3
        anchors.top: label2.bottom
        anchors.topMargin: (parent.height-140)/8
        anchors.right: parent.horizontalCenter
        anchors.rightMargin: 40
        width: 110
        height: 20
        text: qsTr("Altitude")
        verticalAlignment: Text.AlignVCenter
        horizontalAlignment: Text.AlignRight
        font.pixelSize: parent.height/20
    }
    Label {
        id: labelAltitude
        anchors.left: validAltitude.right
        anchors.leftMargin: 10
        anchors.verticalCenter: label3.verticalCenter
        width: 120
        height: 20
        text: qsTr("   -.-")
        verticalAlignment: Text.AlignVCenter
        font.pixelSize: parent.height/20
    }
    Rectangle {
        id: validAltitude
        anchors.left: label3.right
        anchors.leftMargin: 10
        anchors.verticalCenter: label3.verticalCenter
        width: 24
        height: 24
        color: "#ff0000"
        radius: 12
        border.color: "#000000"
        border.width: 2
    }

    //Speed
    Label {
        id: label4
        anchors.top: label3.bottom
        anchors.topMargin: (parent.height-140)/8
        anchors.right: parent.horizontalCenter
        anchors.rightMargin: 40
        y: 155
        width: 110
        height: 20
        text: qsTr("Speed")
        verticalAlignment: Text.AlignVCenter
        horizontalAlignment: Text.AlignRight
        font.pixelSize: parent.height/20
    }
    Label {
        id: labelSpeed
        anchors.left: validSpeed.right
        anchors.leftMargin: 10
        anchors.verticalCenter: label4.verticalCenter
        width: 120
        height: 20
        text: qsTr("   -.-")
        verticalAlignment: Text.AlignVCenter
        font.pixelSize: parent.height/20
    }
    Rectangle {
        id: validSpeed
        anchors.left: label4.right
        anchors.leftMargin: 10
        anchors.verticalCenter: label4.verticalCenter
        width: 24
        height: 24
        color: "#ff0000"
        radius: 12
        border.color: "#000000"
        border.width: 2
    }

    //Direction
    Label {
        id: label5
        anchors.top: label4.bottom
        anchors.topMargin: (parent.height-140)/8
        anchors.right: parent.horizontalCenter
        anchors.rightMargin: 40
        y: 195
        width: 110
        height: 20
        text: qsTr("Direction")
        verticalAlignment: Text.AlignVCenter
        horizontalAlignment: Text.AlignRight
        font.pixelSize: parent.height/20
    }
    Label {
        id: labelDirection
        anchors.left: validDirection.right
        anchors.leftMargin: 10
        anchors.verticalCenter: label5.verticalCenter
        width: 120
        height: 20
        text: qsTr("   -.-")
        verticalAlignment: Text.AlignVCenter
        font.pixelSize: parent.height/20
    }
    Rectangle {
        id: validDirection
        anchors.left: label5.right
        anchors.leftMargin: 10
        anchors.verticalCenter: label5.verticalCenter
        width: 24
        height: 24
        color: "#ff0000"
        radius: 12
        border.color: "#000000"
        border.width: 2
    }

    //Horizontal Accuracy
    Label {
        id: label7
        anchors.top: label5.bottom
        anchors.topMargin: (parent.height-140)/8
        anchors.right: parent.horizontalCenter
        anchors.rightMargin: 40
        width: 110
        height: 20
        text: qsTr("HAccuracy")
        verticalAlignment: Text.AlignVCenter
        horizontalAlignment: Text.AlignRight
        font.pixelSize: parent.height/24
    }
    Label {
        id: labelHAccuracy
        anchors.left: validHAccuracy.right
        anchors.leftMargin: 10
        anchors.verticalCenter: label7.verticalCenter
        width: 120
        height: 20
        text: qsTr("   -.-")
        verticalAlignment: Text.AlignVCenter
        font.pixelSize: parent.height/24
    }
    Rectangle {
        id: validHAccuracy
        anchors.left: label7.right
        anchors.leftMargin: 10
        anchors.verticalCenter: label7.verticalCenter
        width: 24
        height: 24
        color: "#ff0000"
        radius: 12
        border.width: 2
        border.color: "#000000"
    }

    //Vertical Accuracy
    Label {
        id: label8
        anchors.top: label7.bottom
        anchors.topMargin: (parent.height-140)/8
        anchors.right: parent.horizontalCenter
        anchors.rightMargin: 40
        width: 110
        height: 20
        text: qsTr("VAccuracy")
        verticalAlignment: Text.AlignVCenter
        horizontalAlignment: Text.AlignRight
        font.pixelSize: parent.height/24
    }
    Label {
        id: labelVAccuracy
        anchors.left: validVAccuracy.right
        anchors.leftMargin: 10
        anchors.verticalCenter: label8.verticalCenter
        width: 120
        height: 20
        text: qsTr("   -.-")
        verticalAlignment: Text.AlignVCenter
        font.pixelSize: parent.height/24
    }
    Rectangle {
        id: validVAccuracy
        anchors.left: label8.right
        anchors.leftMargin: 10
        anchors.verticalCenter: label8.verticalCenter
        width: 24
        height: 24
        color: "#ff0000"
        radius: 12
        border.width: 2
        border.color: "#000000"
    }

    Rectangle {
        id: rectangle
        anchors.fill: parent
        z: -1
        gradient: Gradient {
            GradientStop {
                position: 0
                color: "#886305"
            }
            GradientStop {
                position: 1
                color: "#fdddb4"
            }
        }
    }
}
