import QtQuick 2.7

Item {
    width: 400
    height: 400
    property alias textSpeedKmH: textSpeedKmH

    Text {
        id: textSpeedKmH
        anchors.centerIn: parent
        text: qsTr("-.-  km/h")
        font.pixelSize: parent.width/6
    }

    Rectangle {
        id: rectangle
        anchors.fill: parent
        z: -1
        gradient: Gradient {
            GradientStop {
                position: 0
                color: "#886305"
            }
            GradientStop {
                position: 1
                color: "#fdddb4"
            }
        }
    }
}
