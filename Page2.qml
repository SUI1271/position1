import QtQuick 2.7

Page2Form {

    checkBoxMovingMap.onCheckedChanged: {
        if (checkBoxMovingMap.checked == true) {
            root.bMovingMap = true
        } else
        {
            root.bMovingMap = false
        }
    }

    button2.onReleased: {
        image2.visible = false
}
    button2.onPressed: {
        image2.visible = true
    }

    button.onReleased: {
        image.visible = false
}
    button.onPressed: {
        image.visible = true
    }
}
