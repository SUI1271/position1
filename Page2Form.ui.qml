import QtQuick 2.7
import QtQuick.Controls 2.2
import QtQuick.Controls.Styles 1.4
import QtQuick.Layouts 1.3

Item {
    width: 640
    height: 480
    property alias checkBoxMovingMap: checkBoxMovingMap
    property alias button: button
    property alias button2: button2
    property alias image: image
    property alias image2: image2

    AnimatedImage {
        id: image
        anchors.bottom: parent.verticalCenter
        anchors.bottomMargin: -20
        width: 100
        height: 100
        visible: false
        anchors.left: parent.horizontalCenter
        anchors.leftMargin: 40
        z: 1
        fillMode: Image.Stretch
        clip: true
        source: "Programmer1.gif"
    }

    AnimatedImage {
        id: image2
        anchors.bottom: parent.verticalCenter
        anchors.bottomMargin: -20
        width: 100
        height: 100
        visible: false
        anchors.right: parent.horizontalCenter
        anchors.rightMargin: 40
        z: 1
        fillMode: Image.Stretch
        clip: true
        source: "Programmer2.gif"
    }

    Rectangle {
        id: rectangle
        anchors.fill: parent
        z: -1
        gradient: Gradient {
            GradientStop {
                position: 0
                color: "#886305"
            }
            GradientStop {
                position: 1
                color: "#fdddb4"
            }
        }
    }

    Button {
        id: button2
        anchors.horizontalCenter: image2.horizontalCenter
        anchors.top: image2.bottom
        anchors.topMargin: 10
        width: 130
        text: qsTr("Pedro Knecht")
        font.pointSize: 11
        checked: false
    }

    Button {
        id: button
        anchors.horizontalCenter: image.horizontalCenter
        anchors.top: image.bottom
        anchors.topMargin: 10
        width: 130
        text: qsTr("Andreas Buehlmann")
        font.pointSize: 11
        checkable: false
    }

    Label {
        id: label
        anchors.horizontalCenter: parent.horizontalCenter
        anchors.bottom: image.top
        anchors.bottomMargin: 40
        width: 381
        height: 13
        color: "#faefd3"
        text: qsTr("GNSS Test Android >5.0  Qt 5.9.3  11-2017")
        horizontalAlignment: Text.AlignHCenter
        font.pointSize: 12
    }

    Label {
        id: label1
        x: 474
        y: 467
        width: 166
        height: 13
        color: "#27262b"
        text: qsTr("Map: esri  2017.11.26.002")
        font.pointSize: 9
        verticalAlignment: Text.AlignBottom
        horizontalAlignment: Text.AlignRight
        anchors.right: parent.right
        anchors.rightMargin: 10
        anchors.bottom: parent.bottom
        anchors.bottomMargin: 10
    }

    Switch {
        id: checkBoxMovingMap
        y: 424
        width: 150
        height: 36
        anchors.left: button2.left
        anchors.bottom: parent.bottom
        anchors.bottomMargin: 20
        text: qsTr("Moving Map")
        anchors.horizontalCenterOffset: 74
        font.pixelSize: 14
        font.bold: false
        spacing: 4
    }

}
