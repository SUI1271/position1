import QtQuick 2.7
import QtLocation 5.9
import QtPositioning 5.8
import QtQuick.Controls 2.2
import QtQuick.Controls.Styles 1.4
import QtQuick.Layouts 1.3

Item {
    width: 400
    height: 400
    property alias labelTitle: labelTitle
    property alias rectangleBackground: rectangleBackground
    property alias labelDirection: labelDirection
    property alias labelSpeed: labelSpeed
    property alias mouseAreaUp: mouseAreaUp
    property alias mouseAreaDown: mouseAreaDown
    property alias validHAccuracy: validHAccuracy
    property alias validDirection: validDirection
    property alias validSpeed: validSpeed
    property alias validAltitude: validAltitude
    property alias validLon: validLon
    property alias validLat: validLat

    Rectangle {
        id: validLat
        anchors.right: validLon.left
        anchors.top:parent.top
        anchors.rightMargin: 5
        anchors.topMargin: 5
        z:2
        width: 16
        height: 16
        color: "red"
        radius: 8
        border.width: 1
        border.color: "#000000"
}

    Rectangle {
        id: validLon
        anchors.right: validAltitude.left
        anchors.top:parent.top
        anchors.rightMargin: 5
        anchors.topMargin: 5
        z:2
        width: 16
        height: 16
        color: "red"
        radius: 8
        border.width: 1
        border.color: "#000000"
}

    Rectangle {
        id: validAltitude
        anchors.right: validSpeed.left
        anchors.top:parent.top
        anchors.rightMargin: 5
        anchors.topMargin: 5
        z:2
        width: 16
        height: 16
        color: "red"
        radius: 8
        border.width: 1
        border.color: "#000000"
}

    Rectangle {
        id: validSpeed
        anchors.right: validDirection.left
        anchors.top:parent.top
        anchors.rightMargin: 5
        anchors.topMargin: 5
        z:2
        width: 16
        height: 16
        color: "red"
        radius: 8
        border.width: 1
        border.color: "#000000"
}

    Rectangle {
        id: validDirection
        anchors.right: validHAccuracy.left
        anchors.top:parent.top
        anchors.rightMargin: 5
        anchors.topMargin: 5
        z:2
        width: 16
        height: 16
        color: "red"
        radius: 8
        border.width: 1
        border.color: "#000000"
}

    Rectangle {
        id: validHAccuracy
        anchors.right: parent.right
        anchors.top:parent.top
        anchors.rightMargin: 5
        anchors.topMargin: 5
        z:2
        width: 16
        height: 16
        color: "red"
        radius: 8
        border.width: 1
        border.color: "#000000"
   }

    MouseArea {
        id: mouseAreaUp
        anchors.top: parent.top
        width: parent.width
        height: 50
        z: 2
    }

    MouseArea {
        id: mouseAreaDown
        anchors.bottom: parent.bottom
        width: parent.width
        height: 50
        z: 2
    }

    Label {
        id: labelTitle
        text: "Position / Speed / Direction"
        anchors.top: parent.top
        anchors.topMargin: 5
        anchors.left: rectangleBackground.left
        anchors.leftMargin: 10
        font.pointSize: 16
        z: 2
    }

    Label {
        id: labelSpeed
        anchors.bottom : rectangleBackground.bottom
        anchors.bottomMargin: 10
        anchors.left: rectangleBackground.left
        anchors.leftMargin: 5
        width: 160
        height: 30
        text: qsTr("1271 km/h")
        horizontalAlignment: Text.AlignRight
        visible: true
        font.pointSize: 30
        z: 2
    }

    Label {
        id: labelDirection
        anchors.bottom: rectangleBackground.bottom
        anchors.bottomMargin: 10
        anchors.left: rectangleBackground.left
        anchors.leftMargin: 190
        width: parent.width/2
        height: 30
        text: qsTr("123 °")
        visible: true
        font.pointSize: 30
        z: 2
    }

    Rectangle {
        id: rectangleBackground
        anchors.left: parent.left
//      anchors.top: parent.top
        y: parent.height-75
        width: parent.width
        height: 75
        color: "#3b3b3b"
        opacity: 0.5
        visible: false
        z: 1
    }

}
