import QtQuick 2.7
import QtLocation 5.9
import QtPositioning 5.8

Page1Form {

    labelLat.text : lat
    labelLon.text : lon
    labelAltitude.text: altitude
    labelSpeed.text: speed
    labelDirection.text: direction
    labelHAccuracy.text: horizontalAccuracy
    labelVAccuracy.text: verticalAccuracy

    validLat.color : validLatColor
    validLon.color : validLonColor
    validAltitude.color : validAltitudeColor
    validSpeed.color : validSpeedColor
    validDirection.color : validDirectionColor
    validHAccuracy.color : validHorizontalAccuracyColor
    validVAccuracy.color : validVerticalAccuracyColor
}

