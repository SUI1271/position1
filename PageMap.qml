import QtQuick 2.7
import QtLocation 5.9
import QtPositioning 5.8
import QtQuick.Controls 2.2
import QtQuick.Controls.Styles 1.4
import QtQuick.Layouts 1.3

PageMapForm {

    validLat.color       : validLatColor
    validLon.color       : validLonColor
    validAltitude.color  : validAltitudeColor
    validSpeed.color     : validSpeedColor
    validDirection.color : validDirectionColor
    validHAccuracy.color : validHorizontalAccuracyColor
    labelSpeed.text      : speedKmH
    labelDirection.text  : direction

    property int iInfoDisplayStatus : 0

    mouseAreaUp.onClicked: {
        rectangleBackground.y = 0
        rectangleBackground.height = 75
        rectangleBackground.opacity = 0.5

        switch (iInfoDisplayStatus)
        {
        case 0:
            labelSpeed.visible = false
            labelDirection.visible = false
            rectangleBackground.visible = false
            labelSpeed.color ="black"
            labelDirection.color ="black"
            labelTitle.color ="black"
            iInfoDisplayStatus = 1
            break
        case 1:
            labelSpeed.visible = true
            labelDirection.visible = true
            iInfoDisplayStatus = 2
            break
        case 2:
            labelSpeed.color = "white"
            labelDirection.color = "white"
            labelTitle.color = "white"
            iInfoDisplayStatus = 0
            rectangleBackground.visible = true
            break
        }
    }

    mouseAreaDown.onClicked: {
        rectangleBackground.y = parent.height-50
        rectangleBackground.height = 50
        rectangleBackground.opacity = 1
        switch (iInfoDisplayStatus)
        {
        case 0:
            labelSpeed.visible = false
            labelDirection.visible = false
            rectangleBackground.visible = false
            labelSpeed.color ="black"
            labelDirection.color ="black"
            labelTitle.color ="black"
            iInfoDisplayStatus = 1
            break
        case 1:
            labelSpeed.visible = true
            labelDirection.visible = true
            iInfoDisplayStatus = 2
            break
        case 2:
            labelSpeed.color = "white"
            labelDirection.color = "white"
            labelTitle.color = "black"
            iInfoDisplayStatus = 0
            rectangleBackground.visible = true
            break
        }
    }

    Rectangle {
        anchors.fill: parent

        //! [Initialize Plugin]
        Plugin {
            id: myPlugin
            name: "esri"
                    // "mapbox" -> Mapbox service
                    // "here" -> HERE Services
                    // "osm" -> OpenStreetMap Services
                    // "esri" -> ESRI Services
        }
        //! [Initialize Plugin]

        //! [Current Location]
        PositionSource {
            id: positionSource
            active: true
            updateInterval: 200 //
            onPositionChanged:  {
                var currentPosition = positionSource.position.coordinate
                map.center = currentPosition

                if (position.directionValid == true){
                    boat1.coordinate = currentPosition
                    if (bMovingMap == false) {
                        boat1.rotation = positionSource.position.direction - map.bearing
                    }
                    else {
                        boat1.rotation = 0
                        map.bearing = positionSource.position.direction
                    }
                    boat1.visible = true
                    positionPoint.visible = false
                }
                else
                {
                    boat1.visible = false
                    positionPoint.coordinate = currentPosition
                    positionPoint.visible = true
                }

            }
        }
       //! [Current Location]

        //! [Places MapItemView]
        Map {
            id: map
            anchors.fill: parent
            plugin: myPlugin
            zoomLevel: 18
            bearing: 0

            MapQuickItem {
                id: boat1;
                anchorPoint.x: image1.width * 0.5
                anchorPoint.y: image1.height * 0.5

                sourceItem: Image {
                   id: image1
                   height: 50
                   width: 25
                   source: "boat.png"
                }
            }

            MapQuickItem {
                id: positionPoint;
                anchorPoint.x: rectangle.width * 0.5
                anchorPoint.y: rectangle.height * 0.5
                visible: false

                sourceItem: Rectangle {
                    id: rectangle
                    width: 20
                    height: 20
                    color: "red"
                    radius: 10
                }
            } //MapQuickItem
        }
    }
}
