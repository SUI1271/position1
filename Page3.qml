import QtQuick 2.7
import QtLocation 5.9
import QtPositioning 5.8
import QtQuick.Controls 2.2
import QtQuick.Controls.Styles 1.4
import QtQuick.Layouts 1.3

Page3Form {

    validLat.color : validLatColor
    validLon.color : validLonColor
    validAltitude.color : validAltitudeColor

    property variant startPosition: locationHifi
    property variant locationHifi: QtPositioning.coordinate( 46.73, 7.65)
    property bool bMeasuring: false


    button.onClicked: {
        if (bMeasuring == false) {
            button.text = "STOP"
            rectangle1.color = "red"
            bMeasuring = true
            startPosition = positionSource.position.coordinate
        }
        else {
            button.text = "START"
            rectangle1.color = "green"
            bMeasuring = false
        }

        startPosition = positionSource.position.coordinate
}

    PositionSource {
        id: positionSource
        active: true
        updateInterval: 1000 // msec

        onPositionChanged:  {
            var currentPosition = positionSource.position.coordinate
            if (bMeasuring == true) {
                var distance = currentPosition.distanceTo(startPosition)
                labelDistance.text = distance.toFixed(0) +" m"
            }
        }
    }
}
