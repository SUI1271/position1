import QtQuick 2.7
import QtLocation 5.9
import QtPositioning 5.8
import QtQuick.Controls 2.2
import QtQuick.Controls.Styles 1.4
import QtQuick.Layouts 1.3

Item {
    id: item1
    width: 400
    height: 400
    property alias validAltitude: validAltitude
    property alias validLon: validLon
    property alias validLat: validLat
    property alias rectangle1: rectangle1
    property alias labelDistance: labelDistance
    property alias button: button

    Label {
        id: labelTitle
        width: 260
        height: 39
        anchors.top: parent.top
        anchors.topMargin: (parent.height-140)/8
        anchors.horizontalCenter: parent.horizontalCenter
        text: qsTr("Measure a Distance")
        horizontalAlignment: Text.AlignHCenter
        font.pointSize: 20
    }

    Label {
        id: labelDistance
        anchors.top: button.bottom
        anchors.topMargin:(parent.height-140)/8
        anchors.horizontalCenter: parent.horizontalCenter
        width: 116
        height: 38
        text: qsTr("- m")
        horizontalAlignment: Text.AlignHCenter
        font.pointSize: 30
    }

    RoundButton {
        id: button
        anchors.top: labelTitle.bottom
        anchors.horizontalCenter: parent.horizontalCenter
        width: 90
        height: 90
        z:2
        text: qsTr("START")
        font.bold: true
        anchors.topMargin: 33
        font.pointSize: 18
    }

    Rectangle {
        id: rectangle
        anchors.fill: parent
        z: -1
        gradient: Gradient {
            GradientStop {
                position: 0
                color: "#886305"
            }
            GradientStop {
                position: 1
                color: "#fdddb4"
            }
        }
    }

    Rectangle {
        id: rectangle1
        anchors.centerIn: button
        color: "green"
        width: 120
        height: 120
        radius: 60
    }
    Rectangle {
        id: validLat
        anchors.right: validLon.left
        anchors.top:parent.top
        anchors.rightMargin: 5
        anchors.topMargin: 5
        z:1
        width: 16
        height: 16
        color: "red"
        radius: 8
        border.width: 1
        border.color: "#000000"
}

    Rectangle {
        id: validLon
        anchors.right: validAltitude.left
        anchors.top:parent.top
        anchors.rightMargin: 5
        anchors.topMargin: 5
        z:1
        width: 16
        height: 16
        color: "red"
        radius: 8
        border.width: 1
        border.color: "#000000"
}

    Rectangle {
        id: validAltitude
        anchors.right: parent.right
        anchors.top:parent.top
        anchors.rightMargin: 5
        anchors.topMargin: 5
        z:1
        width: 16
        height: 16
        color: "red"
        radius: 8
        border.width: 1
        border.color: "#000000"
    }
}
